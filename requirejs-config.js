var config = {
    "map": {
        "*": {
            "home-script": "js/home-script",
            "res": "js/responsive",
            "ibnabmodernizr": "js/modernizr-2.8.3",
            "select2": "js/select2.min",
            "top-menu": "js/top-menu",
            "spare": "js/spare",
            "product-swatch":"js/product",
            "catalog-page":"js/catalog-page",
            "configurable":'Firebear_ConfigurableProducts/web/js/configurable'
        }
    }
};