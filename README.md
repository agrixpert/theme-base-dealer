# README #

Main front-end Magento 2 theme for misteragri.com

### Current Version ###

* 2.0.7

Agrixpert request additions:  

* Fixed specification changes on configurable options change on product page
* Add custom text option on the product page
* Remove the review area
* Concatenate tables of specs & extra information and align
* Make the product description collapsed and expandable
* Solis tiles on the home page directly link to product pages.

### CHANGE LOG ###

### 1.5.2 ###

* Fixes for gallery attribute issue and more styling changes.

### 1.5.3 ###

* Loading main menu and mobile menu from Ajax
* More fixes for mobile menu

### 1.5.4 ###

* Fixes for mobile menu
* Fixes for filter importer
* Fixes for breadcrumb

### 1.5.5 ###

* Added 48 shipping label and expected date the default attributes.

### 1.5.6 ###

* Added attribute filter feature
* Fixes for overlap title
* Fixes for non translated label

### 1.5.7 ###

* Small fixes

### 1.5.8 ###

* Updated amasty finder to 1.8.3
* Fixed errors after amsty finder update
* Fixed errors on product label