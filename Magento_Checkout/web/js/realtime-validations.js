require([
    "jquery",
    'ko',
    'Magento_Ui/js/model/messageList',
    'Magento_Ui/js/modal/alert'
], function ($, ko, globalMessageList, malert) {
    "use strict";

    jQuery(document).ready(function (e) {
        //add the key up event to the street number input
        jQuery(document).on("keyup", 'input[name^=street]', function (event) {
            var regexp = /[a-z]+\s\d+/;

            var elem = jQuery(this);

            if (regexp.test(elem.val())) {
                // Entered house number is valid, so remove the border color
                elem.css("border-color", "");
                jQuery(".error-house-number").remove();
            } else {
                // The entered house number is not valid, so make the input's border red
                elem.css("border-color", "red");
                jQuery(".error-house-number").remove();
                jQuery('<span class="error-house-number">Het lijkt erop dat uw huisnummer verkeerd is ingevuld. Gebruik het formaat: straatnaam 45a</span>').insertAfter(elem);
            }
        });

        //add the key up event to the VAT field
        jQuery(document).on("keyup", 'input[name=vat_id]', function (event) {
            var regexp = /^((AT|at)?(U|u)[0-9]{8}|(BE|be)?0[0-9]{9}|(BG|bg)?[0-9]{9,10}|(CY|cy)?[0-9]{8}L|(CZ|cz)?[0-9]{8,10}|(DE|de)?[0-9]{9}|(DK|dk)?[0-9]{8}|(EE|ee)?[0-9]{9}|(EL|GR|el|gr)?[0-9]{9}|(ES|es)?[0-9A-Za-z][0-9]{7}[0-9A-Za-z]|(FI|fi)?[0-9]{8}|(FR|fr)?[0-9A-Za-z]{2}[0-9]{9}|(GB|gb)?([0-9]{9}([0-9]{3})?|[A-Za-z]{2}[0-9]{3})|(HU|hu)?[0-9]{8}|(IE|ie)?[0-9]S[0-9]{5}(L|l)|(IT|it)?[0-9]{11}|(LT|lt)?([0-9]{9}|[0-9]{12})|(LU|lu)?[0-9]{8}|(LV|lv)?[0-9]{11}|(MT|mt)?[0-9]{8}|(NL|nl)?[0-9]{9}(B|b)[0-9]{2}|(PL|pl)?[0-9]{10}|(PT|pt)?[0-9]{9}|(RO|ro)?[0-9]{2,10}|(SE|se)?[0-9]{12}|(SI|si)?[0-9]{8}|(SK|sk)?[0-9]{10})$/;

            var elem = jQuery(this);

            if (regexp.test(elem.val())) {
                // if the entered vat number is valid, we remove the red border
                elem.css("border-color", "");
                jQuery(".error-vat-number").remove();

                var selectedCountry = $('select[name=country_id]').val();

                if (selectedCountry !== 'NL') {
                    jQuery("#vat_validate").show();
                }
            } else {
                // we show a red border and a message if vat number is not valid
                elem.css("border-color", "red");
                jQuery(".error-vat-number").remove();
                jQuery('<span class="error-vat-number">Dit lijkt onjuist, vul bijvoorbeeld in: NL012345678B01, BE0123456789</span>').insertAfter(elem);
                jQuery("#vat_validate").hide();
            }
        });
    });
});