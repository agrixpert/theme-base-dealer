/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
        [
            'ko',
            'Magento_Checkout/js/model/totals',
            'uiComponent'
        ],
        function (ko, totals, Component) {
            'use strict';

            return Component.extend({
                defaults: {
                    template: 'Magento_Checkout/summary/cart-items'
                },
                totals: totals.totals(),
                getItems: totals.getItems(),
                getItemsQty: function () {
                    return parseFloat(this.totals.items_qty);
                },
                isItemsBlockExpanded: function () {
                    return true;
                }
            });
        }
);