/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
        [
            'Magento_Checkout/js/view/summary/shipping',
            'Magento_Checkout/js/model/quote'
        ],
        function (Component, quote) {
            'use strict';

            return Component.extend({

                /**
                 * @override
                 */
                isCalculated: function () {
                    return !!quote.shippingMethod();
                },
                getShippingAmount: function () {
                    var ship = quote.shippingMethod();
                    return this.getFormattedPrice(ship.amount);
                },
                getGrandTotalInclTax: function () {
                    var totals = quote.getTotals()();
                    return this.getFormattedPrice(totals.base_grand_total);
                }
            });
        }
);