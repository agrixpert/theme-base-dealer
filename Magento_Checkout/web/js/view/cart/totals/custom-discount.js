/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
        [
            'Magento_Checkout/js/view/summary/shipping',
            'Magento_Checkout/js/model/quote'
        ],
        function (Component, quote) {
            'use strict';

            return Component.extend({

                /**
                 * @override
                 */
                getDiscount: function () {
                    var items = quote.getItems();

                    var calPrices = 0;
                    var realPrice = 0;

                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        calPrices += item.custom_price;
                        realPrice += item.product.price;
                    }

                    if (realPrice > calPrices) {
                        return this.getFormattedPrice((realPrice - calPrices));
                    }

                    return 0;
                }
            });
        }
); 