/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
        [
            'ko',
            'underscore',
            'uiComponent',
            'Magento_Checkout/js/model/shipping-service',
            'Magento_Catalog/js/price-utils',
            'Magento_Checkout/js/model/quote',
            'Magento_Checkout/js/action/select-shipping-method',
            'Magento_Checkout/js/checkout-data',
            'jquery'
        ],
        function (
                ko,
                _,
                Component,
                shippingService,
                priceUtils,
                quote,
                selectShippingMethodAction,
                checkoutData,
                $
                ) {
            'use strict';

            return Component.extend({
                defaults: {
                    template: 'Magento_Checkout/cart/shipping-rates'
                },
                isVisible: ko.observable(!quote.isVirtual()),
                isLoading: shippingService.isLoading,
                shippingRates: shippingService.getShippingRates(),
                shippingRateGroups: ko.observableArray([]),
                selectedShippingMethod: ko.computed(function () {
                    return quote.shippingMethod() ?
                            quote.shippingMethod()['carrier_code'] + '_' + quote.shippingMethod()['method_code'] :
                            null;
                }
                ),

                /**
                 * @override
                 */
                initObservable: function () {
                    var self = this;
                    this._super();

                    this.shippingRates.subscribe(function (rates) {
                        self.shippingRateGroups([]);
                        _.each(rates, function (rate) {
                            var carrierTitle = rate['carrier_title'];

                            if (self.shippingRateGroups.indexOf(carrierTitle) === -1) {
                                self.shippingRateGroups.push(carrierTitle);
                            }
                        });
                    });

                    var shipping_method = "tablerate_bestway";

                    var name = "shipping_method=";
                    var decodedCookie = decodeURIComponent(document.cookie);
                    var ca = decodedCookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') {
                            c = c.substring(1);
                        }
                        if (c.indexOf(name) == 0) {
                            shipping_method = c.substring(name.length, c.length);
                        }
                    }

                    var myVar = setInterval(function () {
                        $('[id^=s_method_]').each(function () {
                            if ($(this).val() == shipping_method) {
                                $(this).trigger('click');
                                var elementName = $(this).val();
                                document.cookie = "shipping_method=" + elementName + "; path=/";
                                clearInterval(myVar);
                            }

                            $(this).on("click", function () {
                                var elementName = $(this).val();
                                document.cookie = "shipping_method=" + elementName + "; path=/";
                            });
                        });
                    }, 2000);
                    return this;
                },

                /**
                 * Get shipping rates for specific group based on title.
                 * @returns Array
                 */
                getRatesForGroup: function (shippingRateGroupTitle) {
                    return _.filter(this.shippingRates(), function (rate) {
                        return shippingRateGroupTitle === rate['carrier_title'];
                    });
                },

                /**
                 * Format shipping price.
                 * @returns {String}
                 */
                getFormattedPrice: function (price) {
                    return priceUtils.formatPrice(price, quote.getPriceFormat());
                },

                /**
                 * Set shipping method.
                 * @param {String} methodData
                 * @returns bool
                 */
                selectShippingMethod: function (methodData) {
                    selectShippingMethodAction(methodData);
                    checkoutData.setSelectedShippingRate(methodData['carrier_code'] + '_' + methodData['method_code']);

                    return true;
                },
                getShippingSubtotal: function () {
                    var totals = quote.getTotals()();
                    return totals.base_grand_total;
                },
                shippingLoaded: function () {
                    console.log("LOADEDDDD");
                }
            });
        }
);
