require([
    "jquery",
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/resource-url-manager',
    'mage/storage',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/model/payment/method-converter',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/shipping-save-processor/payload-extender'
], function (
        $,
        ko,
        quote,
        resourceUrlManager,
        storage,
        paymentService,
        methodConverter,
        errorProcessor,
        payloadExtender) {
    "use strict";

    /**
     * Switch the visibility of the vat validation button based on the country selected.
     * We don't use valid validation for Dutch customers. 
     */
    $(document).on("change", 'select[name=country_id]', function (event) {
        var selectedCountry = $('select[name=country_id]').val();

        if (selectedCountry === 'NL') {
            $('.vat_validate').hide();
            $("input[name=vat_id]").val("");
            reloadVat();
        } else {
            $('.vat_validate').show();
            $("#vat_validate").html("Check BTW nummer");
            $("#vat_validate").removeClass("vat_valid");
        }
    });

    function reloadVat() {
        var payload;

        var selectedCountry = $('select[name=country_id]').val();

        if (selectedCountry === 'NL') {
            if (quote.shippingAddress() != null) {
                quote.shippingAddress().vatId = undefined;
            }

            if (quote.billingAddress() != null) {
                quote.billingAddress().vatId = undefined;
            }
        }

        payload = {
            addressInformation: {
                'shipping_address': quote.shippingAddress(),
                'billing_address': quote.billingAddress(),
                'shipping_method_code': quote.shippingMethod()['method_code'],
                'shipping_carrier_code': quote.shippingMethod()['carrier_code']
            }
        };

        payloadExtender(payload);
        $("input[name=vat_id]").addClass("loading");

        return storage.post(
                resourceUrlManager.getUrlForSetShippingInformation(quote),
                JSON.stringify(payload)
                ).done(
                function (response) {

                    var selectedCountry = $('select[name=country_id]').val();

                    if (response.totals.tax_amount === 0) {
                        $("#vat_status").removeClass("hide-vat-valid");
                        $("#vat_status").addClass("show-vat-valid");

                        $("#vat_validate").html("BTW nummer is goedgekeurd.");
                        $("#vat_validate").addClass("vat_valid");
                        $('.error-vat-number').hide();
                    } else {
                        $("#vat_status").removeClass("show-vat-valid");
                        $("#vat_status").addClass("hide-vat-valid");

                        $("#vat_validate").html("Check BTW nummer");
                        $("#vat_validate").removeClass("vat_valid");

                        if (selectedCountry !== "NL") {
                            showError("Probeer opnieuw");
                        }
                    }

                    if (selectedCountry !== "NL") {
                        $('.vat_validate').show();
                    }

                    $("input[name=vat_id]").removeClass("loading");

                    quote.setTotals(response.totals);
                    paymentService.setPaymentMethods(methodConverter(response['payment_methods']));
                }
        ).fail(
                function (response) {
                    errorProcessor.process(response);
                    $("input[name=vat_id]").removeClass("loading");
                    //fullScreenLoader.stopLoader();
                    console.log("Vat validation failed");
                    showError("VAT validation failed.");
                }
        );
    }

    function showError(msg) {
        if ($('#vat_validate').length > 0) {
            $("#vat_validate").html(msg);

            var elem = jQuery("#vat_validate");
            if (jQuery('.error-vat-number').length > 0) {
                jQuery(this).html('Het BTW nummer kon niet worden gevalideerd, probeer het nogmaals of neem contact op met de helpdesk op: 0031 85 40 139 40');
            } else {
                jQuery('<span class="error-vat-number">Het BTW nummer kon niet worden gevalideerd, probeer het nogmaals of neem contact op met de helpdesk op: 0031 85 40 139 40</span>').insertAfter(elem);
            }
        }
    }

    /**
     * We add the vat validation button of click event of the vat input field
     * And toggle it's visiblity based on the country selected.
     * We don't use vat validation for Dutch customers
     */
    $(document).on("click", 'input[name=vat_id]', function (event) {
        if ($('#vat_validate').length <= 0) {

            if (quote.shippingAddress().countryId === "NL") {
                console.log("Tax deduction not available for dutch customers.");
            } else {
                if ($('.vat_validate').length === 0) {
                    var button = $('<button type="button" class="btn vat_validate" id="vat_validate"><i id="vat_status" class="fa fa-check hide-vat-valid"></i>Check BTW nummer</button>');
                    var vatInput = $('input[name=vat_id]');

                    $(button).insertAfter(vatInput);
                } else {
                    $('.vat_validate').show();
                }
            }

            $(document).on('click', "#vat_validate", function () {
                if (quote.shippingAddress().countryId === "NL" || quote.billingAddress().countryId === "NL") {
                    console.log("Tax deduction not available for dutch customers.");

                    quote.shippingAddress().vatId = "";
                    quote.billingAddress().vatId = "";
                } else {
                    $("#vat_validate").hide();
                    reloadVat();
                }
            });
        }
    });
});