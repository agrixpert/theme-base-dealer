require([
    "jquery",
    "jquery/ui"
], function ($) {
    "use strict";

    jQuery(document).ready(function (e) {
        /**
         * copy the fields from billing address to shipping address when unchecked the address same as billing check box
         */
        jQuery(document).on("click", '#billing-address-same-as-shipping', function (event) {
            var firstname = jQuery($("#shipping-new-address-form > .field > .control > input[name=firstname]")).val();
            var lastname = jQuery($("#shipping-new-address-form > .field > .control > input[name=lastname]")).val();
            var street = jQuery($("#shipping-new-address-form > .street > .control > .field > .control > input[name^='street']")).val();
            var city = jQuery($("#shipping-new-address-form > .field > .control > input[name=city]")).val();
            var postcode = jQuery($("#shipping-new-address-form > .field > .control > input[name=postcode]")).val();
            var company = jQuery($("#shipping-new-address-form > .field > .control > input[name=company]")).val();
            var telephone = jQuery($("#shipping-new-address-form > .field > .control > input[name=telephone]")).val();
            var country = jQuery($("#shipping-new-address-form > .field > .control > select[name=country_id]")).val();
            var vatid = jQuery($("#shipping-new-address-form > .field > .control > input[name=vat_id]")).val();

            if (jQuery(this).prop("checked") == false) {
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=firstname]")).val(firstname);
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=lastname]")).val(lastname);
                jQuery($("fieldset.fieldset.address > .street > .control > .field > .control > input[name^='street']")).val(street);
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=city]")).val(city);
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=postcode]")).val(postcode);
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=company]")).val(company);
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=telephone]")).val(telephone);
                jQuery($("fieldset.fieldset.address > .field > .control > select[name=country_id]")).val(country);
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=vat_id]")).val(vatid);
            } else {
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=firstname]")).val("");
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=lastname]")).val("");
                jQuery($("fieldset.fieldset.address > .street > .control > .field > .control > input[name^='street']")).val("");
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=city]")).val("");
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=postcode]")).val("");
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=company]")).val("");
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=telephone]")).val("");
                jQuery($("fieldset.fieldset.address > .field > .control > select[name=country_id]")).val("");
                jQuery($("fieldset.fieldset.address > .field > .control > input[name=vat_id]")).val("");
            }
        });
    });
});