define([
    "jquery",
    "jquery/ui"
], function ($) {
    "use strict";
    return{
        getSpares: function ($partNo, $mainProductId) {
            var AjaxUrl = "/spare/product/spare/part_number/" + $partNo + "/main_product_id/" + $mainProductId;
            $('body').trigger('processStart');
            setTimeout(function () {
                $.ajax({
                    showLoader: true,
                    context: 'body',
                    url: AjaxUrl,
                    type: "POST",
                    data: {part_number: $partNo, main_product_id: $mainProductId},
                }).done(function (data) {
                    $('.group-product-items').html(data.output);
                    $('body').trigger('processStop');
                    $('.group-item-container').show();
                    return true;
                });
            }, 1000);
        }
    }
});