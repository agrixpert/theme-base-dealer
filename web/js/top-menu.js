require(['jquery', 'Magento_Checkout/js/model/full-screen-loader'], function ($, fullScreenLoader) {
    "use strict";

    $(document).ready(function (e) {
        var menuRequest;


        /** handles mobile menu events */
        if (isMobile()) {
            /**** DEVICE IS MOBILE */

            $('.level-top-ajax').on('click', function (e) {
                $('.nav-sections-item-content').prepend('<div class="menu-loader"></div>');
                var catId = $(this).attr('data-catid');
                sendMobileRequest(catId);
                e.stopPropagation();
            });

            function addListeners() {
                $('.ajax-submenu-item').on('click', function (e) {
                    var catId = $(this).attr('data-catid');
                    $('.nav-sections-item-content').prepend('<div class="menu-loader"></div>');
                    sendMobileRequest(catId);
                    e.stopPropagation();
                });
            }

            function sendMobileRequest(cat) {
                $.ajax({
                    url: '/menucontrol/menu/submenu/catid/' + cat,
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        $('.nav-sections-item-content').hide().html(data['success']).slideDown();
                        addListeners();
                        fullScreenLoader.stopLoader();
                    },
                    error: function () {
                        console.log("Error while loading the menu");
                        status = "error";
                    },
                    complete: function () {
                        fullScreenLoader.stopLoader();
                    }
                });
            }

            if ($('.nav-sections-items').children().length <= 0) {
                $('.nav-toggle').fadeOut();
            }

            /** handles the mobile menu header icon clicks */
            // closes the mobile menu by toggling the necessary clases on mobile menu
            jQuery(document).on("click", '.mobile-menu-close-button, .nav-toggle', function (event) {
                $('html').toggleClass('nav-open');
                $('html').toggleClass('nav-before-open');
            });

            // sends the user to account page
            jQuery(document).on("click", '.mobile-menu-login', function (event) {
                window.location.href = "/customer/account/login/";
            });

            sendMobileRequest('root');
        } else {
            /**** DEVICE IS DESKTOP */
            var currentItem;
            var currentCat;

            /**
             * Listen for the mouse enter event of th top level menu item 
             */
            $(".main-menu-top-level").on('mouseenter', function (e) {
                currentItem = $(this); // get the event target element
                currentCat = currentItem.attr('data-dcat'); // get the category id from data attribute

                if (currentItem.children("ul").children().length <= 1) {
                    currentItem.children("ul").prepend('<div class="menu-loader">De categorieën worden geladen</div>').show(); // removing the html content from the container, and add the menu loading animation
                    sendRequest();
                } else {
                    currentItem.children("ul").fadeIn();
                }

                e.preventDefault();
            });

            $(".menu > ul > li").mouseleave(function (e) {
                $(this).children("ul").stop(true, false).fadeOut("fast");
                //$(this).children("ul").empty();
                e.preventDefault();
            });

            function sendRequest() {
                // send the ajax request for the selected category
                // Mr\Agri\Controller\Menu\AjaxMenu

                menuRequest = $.ajax({
                    url: '/menucontrol/menu/ajaxmenu/catid/' + currentCat,
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        console.log("GOT RESULT " + data['catid']);
                        jQuery(".cat-" + data['catid']).html("").html(data['success']);
                        jQuery(".cat-" + data['catid']).height('auto');
                    },
                    error: function () {
                        console.log("Error while loading the menu");
                        currentItem.children("ul").height(50);
                        currentItem.children("ul").html("").hide().append('<div class="menu-reload-button-cont"><span class="menu-load-error">De categorieën konden niet worden geladen</span><span class="menu-reload-button"></span><span class="menu-load-error">Opnieuw proberen</span></div>').fadeIn();

                        $('.menu-reload-button').on('click', function () {
                            sendRequest();
                        });
                    }
                });
            }
        }

        // detect if the current device is mobile or not, return true if yes, else false.
        function isMobile() {
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                if (screen.width < 768) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }
    });
});