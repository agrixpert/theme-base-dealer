require([
    "jquery",
    "jquery/ui",
    'ibnabmodernizr',
    'select2'
], function ($) {
    "use strict";
    let st;
    $('#product_addtocart_form').submit(function(e) {
        var submitForm = true;
        $('div.swatch-attribute').each(function() {
            if (typeof $(this).attr("option-selected") == 'undefined') {
                 showConfigOpts();
                 $(".tmp-notice-select-opts").remove();
                 jQuery(".floatable-add-to-cart").prepend("<span class='tmp-notice-select-opts'>Niet alle opties zijn gekozen</span>");
                 jQuery(".floatable-add-to-cart").css('padding-top', '20px');
                 $(".tmp-notice-select-opts").css('position', 'fixed');
                 submitForm = false;
            }
        })
return submitForm;
});

    function showConfigOpts() {
        
        $('.floatable-add-to-cart').css('max-height', '100%');
        $('body').css('height', '100% !important');
        $('html').css('height', '100% !important');
        $('body').css('overflow-y', 'hidden');

        if ($('.configure-product').is(":visible"))
        // $('.floatable-add-to-cart').addClass("float-opnen");
        $('.configure-product-finish').show();

        $('.configure-product').hide();
        $('.page-header').hide();
        $('.product-options-wrapper h4').show();
        $('.product-options-wrapper').show();
        $('.catalog-product-view .fieldset .field').css('display', 'block');

        st = setInterval(function() {
            $('.catalog-product-view .fieldset .field').css('display', 'block');
            if (!$(".tmp-msg").length) {
                var optionWrapElement = $('.product-options-wrapper');

                if (optionWrapElement[0].scrollHeight > optionWrapElement[0].clientHeight) {
                    $('.product-options-wrapper').append("<div style='color:red' class='mobile-only-c tmp-msg'>Scroll for more..</div>");
                }
            }
        },500);
        $('.product-options-bottom').css('border-top', 'solid #a0a0a0 2px');

    }

    function hideConfigOpts() {
      //  $(".product-options-wrapper .fieldset .field").each(function (k, el) {
       //     $(el).hide();
       // });
        $('.floatable-add-to-cart').css('max-height', '120px');
        $('.configure-product').show();
        $('.page-header').show();
        $('.configure-product-finish').hide();
        $('.product-options-wrapper h4').hide();
        $('.product-options-bottom').css('border-top', 'none');
        $('body').css('overflow-y', 'auto');
        $('.product-options-wrapper').hide()
        clearInterval(st);
        $('.catalog-product-view .fieldset .field').css('display', 'none');
        // $('.floatable-add-to-cart').removeClass("float-opnen");
        $('.tmp-msg').remove();
    }

    $(".configure-product").click(showConfigOpts);
    $(".configure-product-finish").click(hideConfigOpts);


    if ($('.description-field-trunc-wrap').height() > 240) {
            $('.description-field-trunc-wrap').css('overflow-y','hidden');
            $('.description-field-trunc-wrap').css('height','200px');
            $('.expand-desc').css('display','block');
    }

    $('.description-field-trunc-wrap .expand-desc').click(function () {
            $('.expand-desc').hide();
            $('.description-field-trunc-wrap').css('height','auto');
    });
});