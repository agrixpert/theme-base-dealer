require([
    "jquery",
    "jquery/ui",
    'ibnabmodernizr',
    'select2'
], function ($) {
    "use strict";

    jQuery(document).ready(function (e) {
        var h = jQuery(".category-description").height();

        if (h < 100) {
            jQuery(".category-description").css("max-height", "100%");
            jQuery(".category-description").animate({"height": "100%"}, 1000);
            jQuery(".read-more").fadeOut();
        } else {
            jQuery(".read-more").show();
        }

        $(".read-more-trigger").click(function () {
            if ($(".contDiv").height() === 150) {
                $(".contDiv").animate({height: "100%"}, 1000);
            } else {
                $(".contDiv").animate({height: 150}, 1000);
                $(window).scrollTop(0);
            }
        });

        // READ-MORE BUTTON
        jQuery(".read-more-button").click(function () {
            jQuery(".category-description").css("max-height", "100%");
            jQuery(".category-description").animate({"height": "100%"}, 1000);
            jQuery(".read-more").fadeOut();
        });

        $('.amasty-h-form').height($('.amfinder-horizontal').height());
    });
});