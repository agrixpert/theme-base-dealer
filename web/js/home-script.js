require([
    'jquery',
    'jquery/ui',
    'mage/gallery/gallery',
    'ibnabmodernizr'
], function ($) {
    'use strict';

    /** DOCUMENT READY START */
    jQuery(document).ready(function (e) {

        /**************************** HANDLE PLUS AND MINUS BUTTON SITEWIDE *******************/

        jQuery('.plus').on('mouseup', function (event) {
            var target = jQuery(this).prev();
            var qty = parseInt(target.val());

            qty++;

            target.val(qty);

            if (jQuery("#form-validate").length > 0 && isMobile()) {
                jQuery("#form-validate").submit();
            }

            event.stopPropagation();
        });

        jQuery('.minus').on('mouseup', function (event) {
            var target = jQuery(this).next();
            var qty = parseInt(target.val());

            qty -= 1;
            if (qty <= 1) {
                qty = 1;
            }

            target.val(qty);

            if (jQuery("#form-validate").length > 0 && isMobile()) {
                jQuery("#form-validate").submit();
            }

            event.stopPropagation();
        });

        /**************************** HANDLE PLUS AND MINUS BUTTON END *******************/

        if (isMobile()) {
            $(".sidebar-main").appendTo(".filter-container");
        }

        $(".filter-tag").click(function () {
            $(".sidebar-main").toggle("fast", "swing");
            $('.filter-options-content').show();
        });

        $('html').attr('nav-open', "true");

        jQuery(document).on("click", '.show-all-results', function (event) {
            window.location.href = jQuery(":first-child", this).attr("href");
        });

        /* Check if the browser is mobile or desktop, if desktop we expand the layerd navigation by default on page load */
        if (!isMobile()) {
            setTimeout(function () {
                console.log("Desktop view");
                jQuery('.minicart-items').show();
                jQuery('.filter-options-title').addClass('filter-options-title-active'); // showing the down arrow
                jQuery('.filter-options-content').show('fast');
            }, 3000);
        }

        // Make the current page highlight on the root menu.
        jQuery(document).ready(function () {
            jQuery("[href]").each(function () {
                if (this.href == window.location.href) {
                    jQuery(this).addClass("active");
                }
            });
        });

        jQuery(".minicart-wrapper").click(function () {
            var href = jQuery(".showcart").attr("href");

            if (isMobile()) {
                window.location.href = "/checkout/cart/";
            } else {
                window.location.href = "/onestepcheckout/";
            }
        });

        jQuery("#top-cart-btn-checkout").click(function () {
            var href = jQuery(".showcart").attr("href");
            window.location.href = "/onestepcheckout/";
        });



        jQuery(':input[type="number"]').each(function (i, obj) {
            //jQuery(this).spinner();
        });
    });
    /** DOCUMENT READY END */

    // check if the user agent is mobile or not
    function isMobile() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return true;
        } else {
            return false;
        }
    }
});